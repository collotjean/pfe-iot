class Device():
    def __init__(self):
        self.id = 0
        self.typeDevice = ""
        self.puissance = 0
        self.score = 0


class Computer(Device):
    def __init__(self, id):
        super().__init__()
        self.id = id
        self.typeDevice = "Computer"
        self.puissance = 150
        self.score = 5

