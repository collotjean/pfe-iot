## iotdevices.py 
## Juan Angel Lorenzo del Castillo (jlo@cy-tech.fr)
## CY-Tech, CY Cergy Paris Université

import shlex
import sys
import numpy as np


import gym
from gym import logger, spaces
from gym.utils import seeding


class IoTDevice:
    def __init__(self,id,icon=None):
        self.id = str(id)
        self.model = None
        self.icon = icon
        self.score = 1

    def __str__(self):
        return "IoTDevice: {0}".format(self.id)

    def setPerformance(self,performance):
        self.performance = performance

    def setNetwork(self,network):
        self.network = network

    def setModel(self,model=None):
        self.model = model

    def setPower(self,power):
        self.power = power


class Score:
    """ Note yet done. Must think about the score. """
    def __init__(self):
        self.score = 1


class ComputingPerformance():
    def __init__(self,Gflops=None,Flops_watt=None):
        self.Gflops = Gflops
        self.Flops_watt = Flops_watt


class Network():
    def __init__(self,conn_quality,wired=False,drop_probability=0):
        self.wired = wired
        self.conn_quality = conn_quality
        self.drop_probability = drop_probability/100

class Power():
    def __init__(self,battery_level,plugged=True,decay_rate=2):
        self.plugged = plugged
        self.battery_level = battery_level
        if not plugged:
            self.decay_rate = decay_rate
        else:
            self.decay_rate = 0
            self.battery_level = 100


    #def recalculate() according to the battery decay rate


def echo(phrase: str) -> None:
   """A dummy wrapper around print."""
   # for demonstration purposes, you can imagine that there is some
   # valuable and reusable logic inside this function
   print(phrase)

def main() -> int:
    """Echo the input arguments to standard output"""
   # phrase = shlex.join(sys.argv)
    echo(sys.argv)

    perf = ComputingPerformance(Gflops=100)
    iotdevice = IoTDevice(42)
    iotdevice.setPerformance(perf)
    print(iotdevice)
    print(iotdevice.performance.Gflops)
    return 0

if __name__ == '__main__':
    sys.exit(main())