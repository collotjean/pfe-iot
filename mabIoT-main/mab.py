"""
File with few multi-armed bandits algorithms

UCB
"""

import math

def UCB(observations, counts, t):
    """
    arguments:
        observation : array of observations
        counts : array of number of time each action as been selected
        t : number of iteration

    return:
        int -> the position of the chosen action

    description:

    """
    if(counts[1] == 0):
        return 1

    for i in range(len(counts)):
        if(counts[i] == 0):
            return i
    
    ucb_values = [0.0 for _ in counts]
    for i in range(len(observations)):
        ucb_values[i] = observations[i] + math.sqrt(3 * math.log(t) / counts[i])

    ucb_value_max = max(ucb_values)
    return ucb_values.index(ucb_value_max)