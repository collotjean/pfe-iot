## iotenv.py 
## Juan Angel Lorenzo del Castillo (jlo@cy-tech.fr)
## CY-Tech, CY Cergy Paris Université

from math import inf
import numpy as np 
import cv2 
import matplotlib.pyplot as plt
import PIL.Image as Image
import gym
import random
import itertools
import math

from gym import Env, spaces
from iotdevices import *
import time

font = cv2.FONT_HERSHEY_COMPLEX_SMALL 


## Environment for an IoT scenario, such as a building
## We start by generating:
#      - either a random amount of iot devices (from the IoTDevice class) with randomly-generated init values
#      - either some iot devices (from the IoTDevice class) defined by hand

## Observations: execution time (to further complete combining it with the load balancing ratio or similar)
## actions: as a distribution. That is, an array [(0...1),(0...1),0...1).....] for each device according to
#  the amount of load dispatched. We agreed on not taking them as continuous values between 0 and 1, but discrete steps (0.1, 0.2, ..., 0.9, 1)
## Reward: Good question. It is supposed to be a single value...

class IoTenvironment(gym.Env):
    def __init__(self,num_devices=6):
        super(IoTenvironment, self).__init__()
        
        # Define a 2-D space
        #self.observation_shape = (600, 800, 3)
        self.observation_shape = (700, 1000, 3)
        #TODO: modify according to the number of elements to draw
        # What about a grid or sth like the Dict used at the end of this function?

        # Permissible area to draw devices
        self.y_min = int (self.observation_shape[0] * 0.1)
        self.x_min = 0
        self.y_max = int (self.observation_shape[0] * 0.9)
        self.x_max = self.observation_shape[1]

        # Create a canvas to render the environment images upon 
        self.canvas = np.ones(self.observation_shape) * 1


        # Random max performance values to start with
        max_gflops = 100 
        max_flops_watt= 10

        # Define iotdevices present inside the environment
        self.num_devices = num_devices
        self.iotdevices = []

        # Define elements present in the canvasrandom entre o et 1000
        self.elementsInCanvas = []

        # Initialise devices
        for dev_num in range(self.num_devices):
            print('Initialising device',dev_num)
            perf = ComputingPerformance(Gflops=random.randrange(max_gflops,step=5),Flops_watt=random.randrange(max_flops_watt))
            network = Network(conn_quality=round(random.uniform(0.1,1),2),wired=random.choice([True,False]),drop_probability=random.randrange(100))
            power = Power(random.randrange(100),plugged=random.choice([True,False]),decay_rate=random.randrange(100))
            iotdevice = IoTDevice(dev_num)
            iotdevice.setPerformance(perf)
            iotdevice.setNetwork(network)
            iotdevice.setPower(power)
            iotdevice.icon, iotdevice.model = random.choice([("raspberry_pi_3_rev-1.2.png","Rpi4"),("STM32F103C8T6_blue_mini_2.png","STM32"),("nvidia-jetson.png","Jetson")])
            self.iotdevices.append(iotdevice)

            print('-----------------------')
            print('Device',iotdevice,'created with:')
            print('Network: Wired =',iotdevice.network.wired,', conn_quality =',iotdevice.network.conn_quality,', drop_probability =',iotdevice.network.drop_probability)
            print('Performance: Gflops =',iotdevice.performance.Gflops,', flops_watt =',iotdevice.performance.Flops_watt)
            print('Power: Plugged =',iotdevice.power.plugged,', decay_rate =',iotdevice.power.decay_rate,' battery_level =',iotdevice.power.battery_level)
            print('-----------------------')
            print()
            
            # Add the iotdevice graphical representation to the canvas
            iotdev_canvas = IoTdeviceInCanvas(iotdevice,self.x_max,self.x_min,self.y_max,self.y_min)
            self.elementsInCanvas.append(iotdev_canvas)

        #Define actions
        self.actions = list(itertools.product([0,1,0.5,0.25], repeat = self.num_devices))

        def f(x) : 
            return sum(list(x)) == 1

        self.actions = list(filter(f,self.actions))
        """        
        print()
        print("Actions  :")
        #print(self.actions)
        print()"""

        #Define observation
        self.observations = [ [] for i in range(10)]
        for i in range(10):
            self.observations[i] = [1 for i in range(len(self.actions))]

        """     
        print()
        print("Observations  :")
        #print(self.observations)
        print()"""
        


        # Define an observation space with a set of continuous values (execution time, etc.) for each iotdevice
        execution_time = spaces.Box(low = 0, high = inf, shape = (1,), dtype = np.float32)
        load_balancing = spaces.Box(low = 0, high = 1, shape = (1,), dtype = np.float32)
        #observations_tuple = (execution_time,load_balancing) * self.num_devices
        observations_tuple = (spaces.Dict({"execution_time": execution_time, "load_balancing": load_balancing}),) * self.num_devices
        #TODO: Check whether we need to modify the observations_tuple to add battery level, network, etc.

        print('Observations_tuple =',observations_tuple)
        print()
        self.observation_space = spaces.Tuple(observations_tuple)
        #print(self.observation_space.sample())
        #print()

    #calcul the obervation for one step
    def observationCalcul(self,device,action) :
        if(device.network.conn_quality == 1) :
            coefNetwork = 1
        else :
            coefNetwork = - math.log(1-device.network.conn_quality)/5
            
        if(device.network.drop_probability == 0):
            coefDrop = 1
        else:
            coefDrop = - math.log(device.network.drop_probability)/5

        return device.performance.Gflops*coefDrop*coefNetwork*action

    #calcul of the score for one device
    #param action : action done for the step
    #param observationDevice: le device pour lequel on calcul le score
    def scoreCalcul(self,action,observationDevice):
        scoreSum = 0
        observationSum = 0
        oldscore= np.zeros((self.num_devices))
        for i in range(self.num_devices):
            scoreSum += self.iotdevices[i].score
            observationSum += observationDevice[i]

        for i in range(self.num_devices):
            oldscore[i] = self.iotdevices[i].score
            if action[i] != 0:
                self.iotdevices[i].score = self.iotdevices[i].score/scoreSum + self.iotdevices[i].score/scoreSum * observationDevice[i]/observationSum*100

        return oldscore

    #calcul of the rewards for one step
    #param action : action done for the step
    def rewards(self, action):

        observationDevice = [0 for i in range(self.num_devices)]
        for i in range(self.num_devices):
            observationDevice[i] = self.observationCalcul(self.iotdevices[i], action[i])
        
        oldscore = self.scoreCalcul(action,observationDevice)

        rewards = 0
        #print(f"action = {action}")
        for i in range(self.num_devices):
            rewards += action[i]*(self.iotdevices[i].score- oldscore[i])
            #print("Reward{} = {}" .format(i,rewards))

        countNonZeros = np.count_nonzero(action)
        #print(countNonZeros)
        return rewards*countNonZeros

    def step(self,action, i):
        rewards = self.rewards(action)
        self.observations[1][self.actions.index(action)] = rewards
        self.observation = self.observations[1]
        done = True
        return rewards, self.observations, done

    def render(self):
        for i in range(self.num_devices):
            print('Devices ', i, ' :')
            print(self.iotdevices[i].score)


    def draw_elements_on_canvas(self):
        # Init the canvas 
        self.canvas = np.ones(self.observation_shape) * 1

        # Draw the device on canvas
        for elem in self.elementsInCanvas:
            elem_shape = elem.icon.shape
            x,y = elem.x, elem.y
            self.canvas[y : y + elem_shape[0], x:x + elem_shape[1]] = elem.icon

            # Put text infos on canvas
            text_device = '#device: {}\nModel: {}\nBattery: {}% {}\nNetwork: {}% {}\n'\
                    .format(elem.iotdevice.id,elem.iotdevice.model,
                            elem.iotdevice.power.battery_level,
                            "(plugged)" if elem.iotdevice.power.plugged == True else "",
                            elem.iotdevice.network.conn_quality,
                            "(wired)" if elem.iotdevice.network.wired == True else "",
                            )
            n_lines = text_device.count('\n')
           

            #text = 'Fuel Left: {} | Rewards: {}'.format(self.fuel_left, self.ep_return)
            #text = 'Rpi 4'
            #print('x:',x,' y:',y)

            # Put the info on canvas 
            #self.canvas = cv2.putText(self.canvas, text_device, (x,y-10), font, 0.8, (0,0,0), 1, cv2.LINE_AA)
            self.draw_text(text_device, (x,y-(15*n_lines)),fontFace = font, color=(0,0,0), thickness=0, fontScale = 0.75)

            
    
    def draw_text(
        self,
        text,
        uv_top_left,
        color=(255, 255, 255),
        fontScale=0.5,
        thickness=1,
        fontFace=cv2.FONT_HERSHEY_SIMPLEX,
        outline_color=(0, 0, 0),
        line_spacing=1.5,
    ):
        """
        Draws multiline with an outline. 
        Taken from https://gist.github.com/EricCousineau-TRI/596f04c83da9b82d0389d3ea1d782592#file-cv2_puttext_wrapper-py
        """
        assert isinstance(text, str)

        uv_top_left = np.array(uv_top_left, dtype=float)
        assert uv_top_left.shape == (2,)

        for line in text.splitlines():
            (w, h), _ = cv2.getTextSize(
                text=line,
                fontFace=fontFace,
                fontScale=fontScale,
                thickness=thickness,
            )
            uv_bottom_left_i = uv_top_left + [0, h]
            org = tuple(uv_bottom_left_i.astype(int))

            if outline_color is not None:
                cv2.putText(
                    self.canvas,
                    text=line,
                    org=org,
                    fontFace=fontFace,
                    fontScale=fontScale,
                    color=outline_color,
                    thickness=thickness * 3,
                    lineType=cv2.LINE_AA,
                )
            cv2.putText(
                self.canvas,
                text=line,
                org=org,
                fontFace=fontFace,
                fontScale=fontScale,
                color=color,
                thickness=thickness,
                lineType=cv2.LINE_AA,
            )

            uv_top_left += [0, h * line_spacing]


    #TODO: Move here device initialisation???
    def reset(self): 
       
        # Reset the reward
        self.ep_return  = 0

        # Grid canvas 2D dimensions in cartesian (x,y) format
        max_y = 4
        max_x = 4
        x_dev_coord = 0
        y_dev_coord = 0


        # Determine a place to draw the devices in the canvas
        for iotdev_canvas in self.elementsInCanvas:
            #print('x_dev_coord:',x_dev_coord,'x_dev_coord_final:',x_dev_coord // max_x ,'y_dev_coord:',y_dev_coord)
            iotdev_canvas.set_position(x_dev_coord // max_x, y_dev_coord % max_y)
            #print('name for iotdevice',iotdev_canvas.name,' Coordinates',format(iotdev_canvas.get_position()),' True max coordinates:',iotdev_canvas.x_max,iotdev_canvas.x_min,iotdev_canvas.y_max,iotdev_canvas.y_min)
            x_dev_coord += 1
            y_dev_coord += 1

        # Reset the Canvas 
        self.canvas = np.ones(self.observation_shape) * 1

        # Draw elements on the canvas
        self.draw_elements_on_canvas()


        # return the observation
        return self.canvas, self.observations


class Point(object):
    def __init__(self, name, x_max, x_min, y_max, y_min):
        self.x = 0
        self.y = 0
        self.x_min = x_min
        self.x_max = x_max
        self.y_min = y_min
        self.y_max = y_max
        self.name = name 
    
    def set_position(self, x, y):
        gap_factor_y = 40
        gap_factor_x = 140
        self.x = self.clamp((x * self.icon_h) + self.x_min + gap_factor_x * x, self.x_min, self.x_max - self.icon_w)
        self.y = self.clamp((y * self.icon_h) + self.y_min + gap_factor_y * y , self.y_min, self.y_max - self.icon_h)
        #print('self.y_min: ',self.y_min,' self.y_max:',self.y_max,' self.icon_h:',self.icon_h)
    
    def get_position(self):
        return (self.x, self.y)
    
    def move(self, del_x, del_y):
        self.x += del_x
        self.y += del_y
        
        self.x = self.clamp(self.x, self.x_min, self.x_max - self.icon_w)
        self.y = self.clamp(self.y, self.y_min, self.y_max - self.icon_h)

    def clamp(self, n, minn, maxn):
        #print('n:',n,' minn:',minn,' maxn:',maxn,' Clamp: ',max(min(maxn, n), minn))
        return max(min(maxn, n), minn)


class IoTdeviceInCanvas(Point):
    def __init__(self, iotdevice, x_max, x_min, y_max, y_min):
        super(IoTdeviceInCanvas, self).__init__(iotdevice.model,x_max,x_min,y_max,y_min)
        self.iotdevice = iotdevice
        self.icon = cv2.imread(self.iotdevice.icon) / 255.0
        self.icon_w = 64
        self.icon_h = 100
        self.icon = cv2.resize(self.icon, (self.icon_h, self.icon_w))


def main() -> int:
    """Echo the input arguments to standard output"""
   # phrase = shlex.join(sys.argv)
    echo(sys.argv)

    env = IoTenvironment(num_devices=3)

    print('Resetting environment')
    obs = env.reset()

    action = (0.25,0.25,0.5)
    env.step(action,1)
    plt.imshow(obs)
    plt.xlabel("IoT devices observation space")
    frame = plt.gca()

    frame.xaxis.set_label_position('top') 

    frame.axes.xaxis.set_ticks([])
    frame.axes.yaxis.set_ticks([])

    plt.show()

    return 0

if __name__ == '__main__':
    sys.exit(main())