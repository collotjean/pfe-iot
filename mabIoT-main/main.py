from iotenv import IoTenvironment
from matplotlib import pyplot as plt
import itertools
import numpy as np
from mab import UCB
nbTour = 1000


num_devices = 8

env = IoTenvironment(num_devices=num_devices)
actionsChosen = np.array(())

#Define actions
actions = list(itertools.product([0,1,0.5,0.25], repeat = num_devices))

def f(x) : 
    return sum(list(x)) == 1

actions = list(filter(f,actions))
rewardsArray = np.zeros((nbTour))

#recup observation initial
count = [0 for _ in actions]
for i in range(nbTour):
    obs, observations = env.reset()
    rewardsTotal = 0

    """
    plt.imshow(obs)
    plt.xlabel("IoT devices observation space")
    frame = plt.gca()

    frame.xaxis.set_label_position('top') 

    frame.axes.xaxis.set_ticks([])
    frame.axes.yaxis.set_ticks([])

    plt.show()
    """
    for j in range(10) :
        #k = np.random.randint(0,len(actions),1)
        k = UCB(observations[j],count,i*10 + j + 1)
        count[k] += 1
        action = actions[k]
        rewards,observations,done = env.step(action,j)
        rewardsTotal += rewards
        if(j == 9):
            rewardsArray[i] = rewardsTotal

        """
        print('The chosen action is :')
        print(action)
        print("The new scores for the devices are :")
        env.render()
        print("The rewards for this step is :")
        print(rewards)
        print('The total reward is :')
        print(rewardsTotal)
        """

"""
max_count = max(count)
print(actions[count.index(max_count)])

action = actions[count.index(max_count)]
rewards,observations,done = env.step(action,j)
print(rewards)
print(count)
"""
for i in range(5):
    max_count = max(count)
    action = actions[count.index(max_count)]
    rewards,observations,done = env.step(action,0)
    print(f"L'action : {action} a été testé {max_count} fois est à une reward de {rewards}")
    actions.remove(action)
    count.remove(max_count)

print(count)    