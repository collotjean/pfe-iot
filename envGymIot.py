import gym
from gym import spaces
import numpy as np
import pandas as pd
from data import dataInit

class IoTEnvironment(gym.Env) :

    def __init__(self) :
        super(IoTEnvironment).__init__()
        
        #create panda table
        data = []

        for i in range(len(dataInit)):
            new_row = [dataInit[i].id,dataInit[i].typeDevice,dataInit[i].puissance,dataInit[i].score]
            data.append(new_row)

        self.df = pd.DataFrame(data, columns=['Id','type','puissance','score'])
        print("Initialisation de la BDD :")
        print(self.df)

        #action_space
        self.action_space = spaces.Box(
            low=0, high=1, shape=(10,1)
        )

        #observation_space
        self.observation_space = spaces.Box(
            low=0, high=10, shape=(10,1)
        )

        self.ids = []
        self.observations = []

        for i in range(5):
            rand  = np.random.randint(10)

            while(rand in self.ids):
                rand  = np.random.randint(10)   
            self.ids.append(rand)
            self.observations.append(self.df["score"].iloc[i])

        print("Initalisation of the observations")
        print("Ids :")
        print(self.ids)
        print("Scores :")
        print(self.observations)

    def reset(self) :
        print("Reset")
        #update the table with observation_space

        #create a new random env

        #update observation_space

    def calculTemps(self, pourcentage, id):
        if pourcentage == 0 :
            return 0
        puissance = self.df["puissance"].iloc[id]
        temps = pourcentage*(1080*1080)/puissance
        return temps

    def step(self,action) :
        print("Action :")
        print(action)
        #calcul the rewards
        temps = [action[i]*(1080*1080)/self.df["puissance"].iloc[self.ids[i]] for i in range(5)]
        print("Temps :")
        print(temps)
        #send the calcul between the devices

    def render(self, mode='human', close=False) :
        print("render")
        #console render